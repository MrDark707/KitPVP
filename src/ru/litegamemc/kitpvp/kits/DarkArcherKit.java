package ru.litegamemc.kitpvp.kits;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import ru.litegamemc.kitpvp.kits.utils.Kit;

public class DarkArcherKit implements Kit
{
  @Override
  public void giveItems(final PlayerInventory inventory)
  {
    {
      final ItemStack item = new ItemStack(Material.IRON_SWORD);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setItem(0, item);
    }
    {
      final ItemStack item = new ItemStack(Material.BOW);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
      item.setItemMeta(meta);
      inventory.setItemInOffHand(item);
    }
    inventory.setItem(9, new ItemStack(Material.ARROW));
    {
      final ItemStack item = new ItemStack(Material.LEATHER_HELMET);
      final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
      meta.setColor(Color.BLACK);
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setHelmet(item);
    }
    {
      final ItemStack item = new ItemStack(Material.IRON_CHESTPLATE);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setChestplate(item);
    }
    {
      final ItemStack item = new ItemStack(Material.LEATHER_LEGGINGS);
      final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
      meta.setColor(Color.BLACK);
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setLeggings(item);
    }
    {
      final ItemStack item = new ItemStack(Material.IRON_BOOTS);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setBoots(item);
    }
  }
  
  @Override
  public int getHype()
  {
    return 20;
  }
  
  @Override
  public String getName()
  {
    return "DarkArcher";
  }
  
  @Override
  public Material getIcon()
  {
    return Material.STONE_SWORD;
  }
}