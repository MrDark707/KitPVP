package ru.litegamemc.kitpvp.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import ru.litegamemc.kitpvp.kits.utils.Kit;

public class DefenderKit implements Kit
{
  @Override
  public void giveItems(final PlayerInventory inventory)
  {
    {
      final ItemStack item = new ItemStack(Material.IRON_SWORD);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setItem(0, item);
    }
    {
      final ItemStack item = new ItemStack(Material.SHIELD);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setItemInOffHand(item);
    }
    {
      final ItemStack item = new ItemStack(Material.GOLD_HELMET);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setHelmet(item);
    }
    {
      final ItemStack item = new ItemStack(Material.GOLD_CHESTPLATE);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setChestplate(item);
    }
    {
      final ItemStack item = new ItemStack(Material.GOLD_LEGGINGS);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setLeggings(item);
    }
    {
      final ItemStack item = new ItemStack(Material.GOLD_BOOTS);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setBoots(item);
    }
  }
  
  @Override
  public int getHype()
  {
    return 10;
  }
  
  @Override
  public String getName()
  {
    return "Defender";
  }
  
  @Override
  public Material getIcon()
  {
    return Material.SHIELD;
  }
}