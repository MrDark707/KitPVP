package ru.litegamemc.kitpvp.kits.utils;

import org.bukkit.Material;
import org.bukkit.inventory.PlayerInventory;

public interface Kit
{
  void giveItems(final PlayerInventory inventory);
  
  int getHype();
  
  String getName();
  
  Material getIcon();
}