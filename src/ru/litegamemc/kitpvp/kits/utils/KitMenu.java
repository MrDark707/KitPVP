package ru.litegamemc.kitpvp.kits.utils;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import ru.litegamemc.core.menu.utils.Menu;
import ru.litegamemc.core.user.User;
import ru.litegamemc.core.user.UserUtils;
import ru.litegamemc.core.utils.ArrayUtils;
import ru.litegamemc.core.utils.LocaleUtils;
import ru.litegamemc.core.utils.TimerUtils;
import ru.litegamemc.creative.world.WorldManager;
import java.util.HashMap;
import java.util.Map;

public class KitMenu extends Menu
{
  private Map<Integer, Kit> hashMap = new HashMap<>();
  
  public KitMenu(final int hype, final Map<String, String> locale)
  {
    super(54, locale.get("menu.kit.name"));
    TimerUtils.runAsyncTask(() ->
    {
      final Inventory inventory = getInventory();
      final String lore = locale.get("menu.kit.lore");
      int slot = 0;
      for (final Kit kits : KitUtils.list())
      {
        if (inventory.firstEmpty() != -1 && hashMap != null)
        {
          final boolean canSelect = hype >= kits.getHype();
          final ItemStack items = new ItemStack(canSelect ? kits.getIcon() : Material.BARRIER);
          final ItemMeta metas = items.getItemMeta();
          metas.setDisplayName(canSelect ? "§f" + kits.getName() : "§c???");
          metas.setLore(ArrayUtils.toList(lore.replace("{hype}", String.valueOf(kits.getHype()))));
          metas.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
          items.setItemMeta(metas);
          hashMap.put(slot, kits);
          inventory.setItem(slot, items);
          slot++;
        }
        else
          break;
      }
    });
  }
  
  @Override
  public void click(final InventoryClickEvent event)
  {
    event.setCancelled(true);
    if (event.getClickedInventory() == event.getInventory())
    {
      final Kit kit = hashMap.get(event.getSlot());
      if (kit != null)
      {
        final Player player = (Player) event.getWhoClicked();
        final User user = UserUtils.get(player.getUniqueId());
        if (user != null && user.getHype() >= kit.getHype())
        {
          player.setGameMode(GameMode.ADVENTURE);
          player.closeInventory();
          player.teleport(player.getWorld().getSpawnLocation());
          player.setNoDamageTicks(50);
          TimerUtils.runAsyncTask(() ->
          {
            final PlayerInventory playerInventory = player.getInventory();
            WorldManager.giveBaseItems(playerInventory, LocaleUtils.get(player));
            kit.giveItems(playerInventory);
          });
        }
      }
    }
  }
  
  @Override
  public void close(final InventoryCloseEvent event)
  {
    hashMap.clear();
    hashMap = null;
    getInventory().clear();
  }
}