package ru.litegamemc.kitpvp.kits;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import ru.litegamemc.kitpvp.kits.utils.Kit;

public class KnightKit implements Kit
{
  @Override
  public void giveItems(final PlayerInventory inventory)
  {
    {
      final ItemStack item = new ItemStack(Material.WOOD_SWORD);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setItem(0, item);
    }
    {
      final ItemStack item = new ItemStack(Material.IRON_HELMET);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setHelmet(item);
    }
    {
      final ItemStack item = new ItemStack(Material.IRON_CHESTPLATE);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setChestplate(item);
    }
    {
      final ItemStack item = new ItemStack(Material.IRON_LEGGINGS);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setLeggings(item);
    }
    {
      final ItemStack item = new ItemStack(Material.IRON_BOOTS);
      final ItemMeta meta = item.getItemMeta();
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setBoots(item);
    }
  }
  
  @Override
  public int getHype()
  {
    return 5;
  }
  
  @Override
  public String getName()
  {
    return "Knight";
  }
  
  @Override
  public Material getIcon()
  {
    return Material.IRON_CHESTPLATE;
  }
}