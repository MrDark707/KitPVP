package ru.litegamemc.kitpvp.kits;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import ru.litegamemc.kitpvp.kits.utils.Kit;

public class BubzegPidorKit implements Kit
{
  @Override
  public void giveItems(final PlayerInventory inventory)
  {
    {
      final ItemStack item = new ItemStack(Material.END_ROD);
      final ItemMeta meta = item.getItemMeta();
      meta.addEnchant(Enchantment.KNOCKBACK, 2, true);
      meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
      item.setItemMeta(meta);
      inventory.setItem(0, item);
    }
    {
      final ItemStack item = new ItemStack(Material.LEATHER_HELMET);
      final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
      meta.setColor(Color.RED);
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setHelmet(item);
    }
    {
      final ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE);
      final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
      meta.setColor(Color.FUCHSIA);
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setChestplate(item);
    }
    {
      final ItemStack item = new ItemStack(Material.LEATHER_LEGGINGS);
      final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
      meta.setColor(Color.FUCHSIA);
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setLeggings(item);
    }
    {
      final ItemStack item = new ItemStack(Material.LEATHER_BOOTS);
      final LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();
      meta.setColor(Color.FUCHSIA);
      meta.setUnbreakable(true);
      meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
      item.setItemMeta(meta);
      inventory.setBoots(item);
    }
  }
  
  @Override
  public int getHype()
  {
    return 13;
  }
  
  @Override
  public String getName()
  {
    return "BubzegPidor";
  }
  
  @Override
  public Material getIcon()
  {
    return Material.END_ROD;
  }
}
